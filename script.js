const body = document.body;
const boutonRaz = document.getElementById('bouton-raz');
const weatherIcons = document.getElementById('weather-icons');
const icons = Array.from(weatherIcons.getElementsByClassName('weather-icon'));
const question = document.getElementById('question');
const titre = document.getElementById('titre');

// Ce préfixe sera utilisé pour le stockage des paramètres dans le cache
let prefixeAppli = 'meteo-sur-eni';

// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);

// Adaptations de l'interface pour Openboard
if (openboard){
    document.body.classList.add('openboard');
}

// Lecture du stockage local
checkStockageLocal();

async function checkStockageLocal() {

    let url = window.location.search;
    let urlParams = new URLSearchParams(url);
    let valeurARecuperer;

    valeurArecuperer=false;

    // On récupère le titre stocké
    valeurArecuperer = urlParams.get('titre') || await litDepuisStockage('titre');
    if (valeurArecuperer) {
        titre.innerHTML = decodeURIComponent(valeurArecuperer);
    }

    valeurArecuperer=false;
    // On récupère la question stockée
    valeurArecuperer = urlParams.get('question') || await litDepuisStockage('question');
    if (valeurArecuperer) {
        question.innerHTML = decodeURIComponent(valeurArecuperer);
    }

    memorise(titre);
    memorise(question);

}




function displayWeather(weather) {

    boutonRaz.classList.remove('hide');
    
    for (var i = 0; i < icons.length; i++) {
      if (icons[i].getAttribute('alt') !== weather) {
        icons[i].classList.add('hide');
      }
    }
    body.className='';
    body.classList.add(weather);

}

function raz() {
    body.className='';
    icons.forEach(element => {
        element.classList.remove('hide');
    });
    boutonRaz.classList.add('hide');
}

function memorise(conteneur) {
  let texte = conteneur.innerHTML.replace(/<br>/g, '')
  //conteneur.innerHTML = texte;
  stocke(conteneur.id,texte);
  majUrl(conteneur.id,texte);
}


async function litDepuisStockage(cle) {
  console.log('Lecture de la clé ' + cle);

  let valeurAretourner;
  if (openboard) { // Récupération pour Openboard
      try {
          valeurAretourner = await window.sankore.async.preference(prefixeAppli + '-' + cle);
          console.log("lecture " + cle + "=" + valeurAretourner); // Pour la console
      } catch (error) {
          console.error('Erreur lors de la lecture de la clé ' + cle + ' depuis Openboard:', error);
      }
  } else { // Récupération en Web
      valeurAretourner = localStorage.getItem(prefixeAppli + '-' + cle);
      console.log("lecture depuis stockage " + cle + " = " + valeurAretourner); // Pour la console
  }

  return valeurAretourner;
}

function stocke(cle,valeur){

  console.log("stockage "+cle+"="+valeur);

  if (openboard){
  window.sankore.setPreference(prefixeAppli+'-'+cle,valeur);

  } else {
  localStorage.setItem(prefixeAppli+'-'+cle,valeur);
  }    
}

function majUrl(cle, valeur) {
    // Récupère l'URL actuelle
    const url = new URL(window.location);

    // Met à jour ou ajoute le paramètre
    url.searchParams.set(cle, valeur);

    // Modifie l'URL sans recharger la page
    window.history.replaceState({}, '', url);
}